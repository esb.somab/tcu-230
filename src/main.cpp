#include <Arduino.h>
#include <SD.h>
#include <DHT.h>
#include <Keypad.h>
#include <SD.h>
#include "RTClib.h"
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define DHTPIN1 2           // what pin we're connected to
#define DHTPIN2 3           // what pin we're connected to
#define DHTPIN3 4           // what pin we're connected to
#define DHTTYPE DHT22       // DHT 22  (AM2302)
DHT dht1(DHTPIN1, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino
DHT dht2(DHTPIN2, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino
DHT dht3(DHTPIN3, DHTTYPE);
File myFile;
float hum1;  //Stores humidity value
float temp1; //Stores temperature value
float hum2;  //Stores humidity value
float temp2; //Stores temperature value
float hum3;  //Stores humidity value
float temp3; //Stores temperature value
void setup()
{
  Serial.begin(9600);
  Serial.print("Initializing SD card...");
  // On the Ethernet Shield, CS is pin 4. It's set as an output by default.
  // Note that even if it's not used as the CS pin, the hardware SS pin
  // (10 on most Arduino boards, 53 on the Mega) must be left as an output
  // or the SD library functions will not work.
  pinMode(10, OUTPUT);

  dht1.begin();
  dht2.begin();
  dht3.begin();
  if (!SD.begin(10))
  {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open("Florentin.txt", FILE_WRITE);
  hum1 = dht1.readHumidity();
  temp1 = dht1.readTemperature();
  hum2 = dht2.readHumidity();
  temp2 = dht2.readTemperature();
  hum3 = dht3.readHumidity();
  temp3 = dht3.readTemperature();
  // if the file opened okay, write to it:
  if (myFile)
  {
    Serial.print("Writing to test.txt...");

    myFile.println("Humidity#1 : ");
    myFile.println(hum1);
    myFile.println(" %, Temp#1 : ");
    myFile.println(temp1);
    myFile.println(" Celsius");

    myFile.println("Humidity#2 : ");
    myFile.println(hum2);
    myFile.println(" %, Temp#2 : ");
    myFile.println(temp2);
    myFile.println(" Celsius");

    myFile.println("Humidity#3 : ");
    myFile.println(hum3);
    myFile.println(" %, Temp#3 : ");
    myFile.println(temp3);
    myFile.println(" Celsius");
    delay(3000); //Delay 2 sec.
                 // close the file:
    myFile.close();
    Serial.println("done.");
  }
  else
  {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
}

void loop()
{
  // nothing happens after setup
}